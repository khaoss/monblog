---
title: "A propos"
date: "2021-05-22"
---

Ce site est un exmple de l'utilisation de [**blogdown**](https://github.com/rstudio/blogdown). Le thème a été principalement conçu par [@jrutheiser/hugo-lithium-theme](https://github.com/jrutheiser/hugo-lithium-theme), puis modifié par [Yihui Xie](https://github.com/yihui/hugo-lithium-theme).
